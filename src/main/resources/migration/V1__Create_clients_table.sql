CREATE TABLE clients
(
    id           int NOT NULL AUTO_INCREMENT,
    full_name    varchar(128),
    abbreviation varchar(6),
    type         varchar(128),
    PRIMARY KEY (id)
)