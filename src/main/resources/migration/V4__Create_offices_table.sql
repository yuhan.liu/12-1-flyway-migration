CREATE TABLE offices
(
    id      int NOT NULL AUTO_INCREMENT,
    country varchar(128),
    city    varchar(128),
    PRIMARY KEY (id)
)