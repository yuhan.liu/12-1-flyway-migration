ALTER TABLE contract_service_offices
    ADD FOREIGN KEY (office_id) REFERENCES offices (id);
ALTER TABLE contract_service_offices
    ADD FOREIGN KEY (contract_id) REFERENCES contracts (id);
ALTER TABLE contract_service_offices
    ADD FOREIGN KEY (admin_id) REFERENCES staff (id);