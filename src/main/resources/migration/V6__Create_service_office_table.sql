CREATE TABLE contract_service_offices
(
    id          int NOT NULL AUTO_INCREMENT,
    office_id   int,
    contract_id int,
    admin_id    int,
    PRIMARY KEY (id)
)