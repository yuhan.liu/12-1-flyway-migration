CREATE TABLE staff
(
    id         int NOT NULL,
    first_name varchar(128),
    last_name  varchar(128),
    office_id  int,
    PRIMARY KEY (id),
    FOREIGN KEY (office_id) REFERENCES offices (id)
)