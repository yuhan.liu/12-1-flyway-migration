CREATE TABLE contracts
(
    id        int NOT NULL AUTO_INCREMENT,
    name      varchar(128),
    client_id int,
    PRIMARY KEY (id),
    FOREIGN KEY (client_id) REFERENCES clients (id)
)
